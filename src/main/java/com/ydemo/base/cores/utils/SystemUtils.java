package com.example.utils;

import java.util.Calendar;

/**
 * Created by hongbi on 17/3/31.
 */
public class SystemUtils {
    public static synchronized String getUniqueOrderid() {
        Calendar cal = Calendar.getInstance();
        String year = String.valueOf(cal.get(Calendar.YEAR)).substring(2);
        String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
        month = (month.length() <= 1) ? ("0" + month) : month;
        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        day = (day.length() <= 1) ? ("0" + day) : day;
        String hour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
        hour = (hour.length() <= 1) ? ("0" + hour) : hour;
        String minute = String.valueOf(cal.get(Calendar.MINUTE));
        minute = (minute.length() <= 1) ? ("0" + minute) : minute;
        String uniqueNumber = new StringBuffer(year).append(month).append(day).append(hour).append(minute)
                .append(String.valueOf(Math.random() * 3).substring(2, 5)).toString();

        return uniqueNumber;

    }
}
